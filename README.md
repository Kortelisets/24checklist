# Checklist

Generate checklists on any object for any context.

## Sfdx setup

* Push source : sfdx force:source:push -u Checklist
* Assign permission set : sfdx force:user:permset:assign -n Checklist_Manager -u Checklist

## Test Data

* Generate test data : run ChecklistTestData.baseSetup(); 
* => This generates checks and checklists but without filter rules or records to test the checklist instantiation :
* A tab/object ChecklistTests exists to execute tests. Use this one to instantiate checklists. There is a text field and dropdown which can be used to create filter rules in the checklist.

## Run the app

* App : Checklist Manager

## Package

https://trailhead.salesforce.com/en/content/learn/modules/unlocked-packages-for-customers/build-your-first-unlocked-package


### Create a new version
``` 
sfdx force:package:version:create -p "24Checklist" -k 24SuiteBlowsYourHeadOff --wait 10 -v SfyDevHub -c
```

### Install Package
``` 
sfdx force:package:install --wait 10 --publishwait 10 --package 24Checklist@[VERSION] -k 24SuiteBlowsYourHeadOff -r -u [USER]
```

### Create Package
``` 
sfdx force:package:create --name "24Checklist" --path="force-app" --packagetype Managed --targetdevhubusername SfyDevHub
```